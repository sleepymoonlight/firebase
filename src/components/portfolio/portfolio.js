import template from './portfolio.html';
import './portfolio.scss';
import Isotope from 'isotope-layout';
import imagesLoaded from 'imagesloaded';

document.getElementById('section__portfolio').innerHTML = template();
const iso = new Isotope('.portfolio-items', {
    itemSelector: '.image-container',
    layoutMode: 'fitRows'
});
const portfolioGallery = document.querySelector('.portfolio-items');
const filterButton = document.querySelector('.navigation');

filterButton.addEventListener('click',  (event) => {
    const filterValue = event.target.getAttribute('data-filter');
    iso.arrange({filter: filterValue});
});
imagesLoaded(portfolioGallery).on('progress', () => {
    iso.layout()
});
const navigation = document.getElementById('navigation');
const button = navigation.getElementsByClassName('button');
for (let i = 0; i < button.length; i++) {
    button[i].addEventListener('click', function () {
        const current = document.getElementsByClassName('active');
        current[0].className = current[0].className.replace(' active', '');
        this.className += ' active';
    });
}
