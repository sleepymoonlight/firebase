import template from './skills.html';
import './skills.scss'

document.getElementById('section__skills').innerHTML = template();

    const button = document.getElementById('submitButton');
    button.addEventListener('click', function () {
        let skill = document.getElementById("skillName").value;
        let skillRange = document.getElementById('skillRange').value;
        let skillBar = document.createElement('div');
        skillBar.className = 'userRange';
        skillBar.style = ('width:' + skillRange + '%;');
        skillBar.innerHTML += '<p style = "color: white;">' + skill + '</p>';
        document.getElementById('newSkill').insertBefore(skillBar, null);
    });
