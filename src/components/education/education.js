import template from './education.html';
import './education.scss';

document.getElementById('section__education').innerHTML = template();

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBOdUKXRovjvAJ_BeE6W3qTd5o5v9Xk9fA",
    authDomain: "js-fundamentals-firebase.firebaseapp.com",
    databaseURL: "https://js-fundamentals-firebase.firebaseio.com",
    projectId: "js-fundamentals-firebase",
    storageBucket: "js-fundamentals-firebase.appspot.com",
    messagingSenderId: "688753607408"
};
firebase.initializeApp(config);

//initialize data base
const db = firebase.firestore();

db.settings({ timestampsInSnapshots: true });

const educationData = document.querySelector('#educationData');

// create element and render data
function renderEducation(doc) {

    //create elements ann add them class name
    let dataContainer = document.createElement('div');
    dataContainer.classList.add('data__container');
    let date = document.createElement('div');
    date.classList.add('data__date');
    let textContainer = document.createElement('div');
    textContainer.classList.add('data__text');
    let someText = document.createElement('p');
    let title = document.createElement('h3');

    //put data to elements
    dataContainer.setAttribute('data-id', doc.id);
    date.textContent = doc.data().date;
    someText.textContent = doc.data().someText;
    title.textContent = doc.data().title;

    //append child elements
    textContainer.appendChild(title);
    textContainer.appendChild(someText);
    dataContainer.appendChild(date);
    dataContainer.appendChild(textContainer);
    educationData.appendChild(dataContainer);
}
db.collection('education').get().then((snapshot) => {
    snapshot.docs.forEach(doc => {
        renderEducation(doc);
    })
});






