import template from './navigation.html';
import './navigation.scss'

document.getElementById('section__navigation').innerHTML = template();

const openButton = document.getElementById('openButton');
const closeButton = document.getElementById('closeButton');

openButton.addEventListener('click', function () {
    document.getElementById('menu').style.width = "300px";
});

closeButton.addEventListener('click',function () {
    document.getElementById('menu').style.width = "0";
} );

const menu = document.getElementById('menuList');
const element = menu.getElementsByClassName('menu__list--item');
for (let i = 0; i < element.length; i++) {
    element[i].addEventListener('click', function () {
        const current = document.getElementsByClassName('active');
        current[0].className = current[0].className.replace(' active', '');
        this.className += ' active';
        document.getElementById('menu').style.width = "0";
    });
}