import template from './go-top.html';
import './go-top.scss';

document.getElementById('section__go-top').innerHTML = template();



window.addEventListener('scroll', onScroll);

function onScroll() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("goTop").style.display = "block";
    } else {
        document.getElementById("goTop").style.display = "none";
    }
}
const goTopButton = document.getElementById('goTop');

goTopButton.addEventListener("click", function () {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});